# WordPress Migrate SQL
WordPress Migrate SQL allows you to create customized migrations based on
Wordpress SQL sites. This module is recommended in the case is needed to
migrate complex WordPress sites and also for those who feel more comfortable
developing all the migration using a SQL source.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/wordpress_migrate_sql).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/wordpress_migrate_sql).

## Table of contents
- Requirements
- Installation
- Configuration
- Maintainers

## Requirements
- This module requires no modules outside of Drupal core.

## Installation
- Install as you would normally install a contributed Drupal module. For further
  information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration
- This module doesn't have any configuration as of now. When the module enabled
  it allows you to migrate the Wordpress site using SQL.

## Maintainers
- Omar Mohamad - El Hassan Lopesino - [omarlopesino](https://www.drupal.org/u/omarlopesino)
- Alejandro Cabarcos - [alejandro-cabarcos](https://www.drupal.org/u/alejandro-cabarcos)
