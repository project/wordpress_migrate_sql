<?php

namespace Drupal\wordpress_migrate_sql\Plugin\migrate\source;

use Drupal\migrate\Row;

/**
 * Extract content from Wordpress site.
 *
 * @MigrateSource(
 *   id = "wordpress_migrate_sql_posts"
 * )
 */
class Posts extends SqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = $this->select('posts', 'p');

    $query
      ->fields('p', [
        'id',
        'post_date',
        'post_title',
        'post_content',
        'post_excerpt',
        'post_modified',
        'post_name',
        'post_parent',
        'post_status',
        'post_type',
      ]);

    $query->condition('p.post_type', (array) $this->getPostType(), 'IN');
    if (isset($this->configuration['filter']['status']) && is_array($this->configuration['filter']['status'])) {
      $query->condition('p.post_status', $this->configuration['filter']['status'], 'IN');
    }

    if (isset($this->configuration['filter']['terms']) && is_array($this->configuration['filter']['terms'])) {
      $query->join('term_relationships', 'tr', 'tr.object_id = p.id');
      $query->condition('tr.term_taxonomy_id', $this->configuration['filter']['terms'], 'IN');
    }

    if (isset($this->configuration['filter']['terms_exclude']) && is_array($this->configuration['filter']['terms_exclude'])) {
      $query->condition('p.id', $this->getContentUsingTerms($this->configuration['filter']['terms_exclude']), 'NOT IN');
    }

    if (isset($this->configuration['filter']['posts_exclude']) && is_array($this->configuration['filter']['posts_exclude'])) {
      $query->condition('p.id', $this->configuration['filter']['posts_exclude'], 'NOT IN');
    }

    if (isset($this->configuration['filter']['post_parent']) && is_array($this->configuration['filter']['post_parent'])) {
      $query->condition('p.post_parent', $this->configuration['filter']['post_parent'], 'IN');
    }

    $query->groupBy('p.id');
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $meta_properties = $this->configuration['meta_properties'] ?? [];
    return [
      'id'            => $this->t('Post ID'),
      'post_title'    => $this->t('Title'),
      'post_excerpt'  => $this->t('Excerpt'),
      'post_content'  => $this->t('Content'),
      'post_parent'  => $this->t('Post parent'),
      'post_status'  => $this->t('Post Status'),
      'post_date'     => $this->t('Created Date'),
      'post_modified' => $this->t('Modified Date'),
      'path_name'     => $this->t('URL Alias'),
      'post_type'     => $this->t('Post type'),
    ] + array_combine($meta_properties, $meta_properties);
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'id' => [
        'type'  => 'integer',
        'alias' => 'p',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $result = parent::prepareRow($row);
    if ($result) {
      $vocabularies = array_unique(array_merge(['category', 'post_tag'], $this->configuration['term_vocabularies'] ?? []));
      foreach ($vocabularies as $vocabulary) {
        $row->setSourceProperty(sprintf('terms_%s', $vocabulary), $this->getTaxonomyTerms($row, $vocabulary));
      }

      $row->setSourceProperty('thumbnail_id', $this->getThumbnail($row));
      $meta_properties = $this->configuration['meta_properties'] ?? [];
      foreach ($meta_properties as $wordpress_field_name) {
        $meta_value = $this->getPostMetaValue($row->get('id'), $wordpress_field_name);
        if (is_array($meta_value) && count($meta_value) == 1) {
          $meta_value = reset($meta_value);
        }
        elseif (empty($meta_value)) {
          $meta_value = NULL;
        }
        $row->setSourceProperty($wordpress_field_name, $meta_value);
      }
    }
    return $result;
  }

  /**
   * Get the related tags with this post.
   *
   * @param \Drupal\migrate\Row $row
   *   Row.
   * @param string $taxonomy
   *   Machine name of the taxonomy.
   *
   * @return array
   *   List of related tags.
   */
  protected function getTaxonomyTerms(Row $row, string $taxonomy) {
    $query = $this->select('term_relationships', 'tr')
      ->fields('t', ['term_id', 'name']);
    $query->join(
      'term_taxonomy',
      'tt',
      'tr.term_taxonomy_id = tt.term_taxonomy_id'
    );
    $query->join(
      'terms',
      't',
      'tt.term_id = t.term_id'
    );
    $query->condition('tt.taxonomy', $taxonomy)
      ->condition('tr.object_id', $row->get('id'));

    return array_values($query->execute()->fetchAllKeyed(0, 0));
  }

  /**
   * Get the related thumbnail with this post.
   *
   * @param \Drupal\migrate\Row $row
   *   Row.
   *
   * @return array
   *   Thumbnail ID.
   */
  protected function getThumbnail(Row $row) {
    return $this->getPostMetaValue($row->get('id'), '_thumbnail_id');
  }

  /**
   * Get the related Metavalue with this post.
   *
   * @param string $post_id
   *   Post ID.
   * @param string $meta_key
   *   Meta key.
   *
   * @return array
   *   Metavalue.
   */
  protected function getPostMetaValue(string $post_id, string $meta_key) {
    $query = $this->select('posts', 'p');
    $query->addField('pm', 'meta_value');
    $query->join(
      'postmeta',
        'pm',
        'p.id = pm.post_id and pm.meta_key = :meta_key',
        [':meta_key' => $meta_key],
      );
    $query->condition('p.id', $post_id);
    return array_values($query->execute()->fetchAllKeyed(0, 0));
  }

  /**
   * Get Wordpress post type from the migration template.
   */
  protected function getPostType() {
    return !empty($this->configuration['post_type']) ? $this->configuration['post_type'] : 'post';
  }

  /**
   * Get the list of content using a specific term ID.
   */
  protected function getContentUsingTerms(array $terms) {
    $query = $this->select('posts', 'p');
    $query->fields('p', ['id']);
    $query->join('term_relationships', 'tr', 'p.id = tr.object_id');
    $query->condition('tr.term_taxonomy_id', $terms, 'IN');
    /** @var \Drupal\Core\Database\StatementInterface<int> $result */
    $result = $query->execute();
    return $result->fetchAllKeyed(0, 0);
  }

}
