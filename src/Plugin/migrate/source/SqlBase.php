<?php

namespace Drupal\wordpress_migrate_sql\Plugin\migrate\source;

use Drupal\migrate\Plugin\migrate\source\SqlBase as DrupalSqlBase;

/**
 * Based table for source wordpress plugin.
 */
abstract class SqlBase extends DrupalSqlBase {
}
