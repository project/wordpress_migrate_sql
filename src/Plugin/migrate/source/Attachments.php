<?php

namespace Drupal\wordpress_migrate_sql\Plugin\migrate\source;

use Drupal\migrate\Row;

/**
 * Extract attachments from WordPress site.
 *
 * @MigrateSource(
 *   id = "wordpress_migrate_sql_attachments"
 * )
 */
class Attachments extends SqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = $this->select('posts', 'p');
    $query->fields('p', ['id', 'post_date']);
    $query->addField('p', 'post_title', 'title');
    $query->addField('pm', 'meta_value', 'file_path');
    $query->join(
      'postmeta',
      'pm',
      'p.id = pm.post_id and pm.meta_key = :attached_file',
      [':attached_file' => '_wp_attached_file'],
    );

    if (isset($this->configuration['filter']['mime_type']) && is_array($this->configuration['filter']['mime_type'])) {
      $query->condition('p.post_mime_type', $this->configuration['filter']['mime_type'], 'IN');
    }

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'id'   => $this->t('Attachment ID'),
      'post_date' => $this->t('Media Uploaded Date'),
      'title' => $this->t('Post title'),
      'file_path'  => $this->t('File Path'),
      'file_name'  => $this->t('File Name'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'id' => [
        'type'  => 'integer',
        'alias' => 'p',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $result = parent::prepareRow($row);

    if ($result) {
      $row->setSourceProperty('file_name', basename($row->getSourceProperty('file_path')));
    }
    return $result;
  }

}
