<?php

namespace Drupal\wordpress_migrate_sql\Plugin\migrate\source;

/**
 * Extract content from Wordpress site.
 *
 * @MigrateSource(
 *   id = "wordpress_migrate_sql_taxonomy_terms"
 * )
 */
class TaxonomyTerms extends SqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = $this->select('terms', 't');
    $query
      ->fields('t', [
        'term_id',
        'name',
      ])
      ->fields('tt', [
        'taxonomy',
        'parent',
      ]);

    $query->join('term_taxonomy', 'tt', 't.term_id = tt.term_id');
    if (isset($this->configuration['taxonomy'])) {
      $query->condition('taxonomy', $this->configuration['taxonomy'], 'IN');
    }
    $query->orderBy('t.term_id');

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'term_id' => $this->t('Term id'),
      'name' => $this->t('Name'),
      'parent' => $this->t('Parent'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'term_id' => [
        'type'  => 'integer',
        'alias' => 't',
      ],
    ];
  }

}
