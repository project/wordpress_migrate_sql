<?php

namespace Drupal\wordpress_migrate_sql\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Replace wordpress tokens with meta values.
 *
 * Example:
 *
 * Meta field: https://example.com
 * Wordpress field value: <a href='[field="BUTTON_URL"]'>Button</a>
 * Processed value: <a href='[field="https://example.com"]'>Button</a>
 *
 * @MigrateProcessPlugin(
 *   id = "wordpress_meta_property_replace",
 * )
 */
final class WordpressMetaPropertyReplace extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $meta_properties = $this->configuration['meta_properties'] ?? [];

    foreach ($meta_properties as $meta_field) {
      if ($row->hasSourceProperty($meta_field)) {
        $value = str_replace(sprintf('[field "%s"]', $meta_field), $row->get($meta_field), $value);
      }
    }
    return $value;
  }

}
