<?php

namespace Drupal\wordpress_migrate_sql\Drush\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drupal\Core\Database\Database;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drush\Attributes as CLI;
use Drush\Commands\DrushCommands;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Mime\MimeTypeGuesserInterface;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 */
final class WordpressMigrateSqlFilesCommands extends DrushCommands {

  use StringTranslationTrait;

  public function __construct(protected MimeTypeGuesserInterface $mimeTypeGuesser) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('file.mime_type.guesser'),
    );
  }

  /**
   * Command description here.
   */
  #[CLI\Command(name: 'wordpress_migrate_sql:analysys-attachments', aliases: ['wms:af'])]
  #[CLI\Argument(name: 'database_name', description: 'Database name.')]
  #[CLI\Usage(name: 'wordpress_migrate_sql:analysys-attachments legacy', description: 'Analize files inside')]
  public function analyzeAttachments($database_name) {
    $database = Database::getConnection('default', $database_name);

    $attachments_query = $database->select('posts', 'p')
      ->fields('p', ['post_mime_type']);

    $attachments_query->addExpression('COUNT(p.id)', 'total');
    $attachments_query->condition('p.post_type', 'attachment');
    $attachments_query->groupBy('p.post_mime_type');

    /** @var \Drupal\Core\Database\StatementInterface<int> $attachments_query_result */
    $attachments_query_result = $attachments_query->execute();
    $attachments = $attachments_query_result->fetchAllKeyed();

    $attachments_table = [
      [
        $this->t('Mime Type'),
        $this->t('Total'),
      ],
    ];

    /** @var \Drush\Log\DrushLoggerManager $logger */
    $logger = $this->logger();
    $logger->success($this->t('Total files found: :total', [':total' => array_sum($attachments)]));

    foreach ($attachments as $mime_type => $total) {
      $attachments_table[] = [
        $mime_type,
        $total,
      ];
    }

    return new RowsOfFields($attachments_table);
  }

}
